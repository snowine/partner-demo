# Patner Demo

## Overview

The repository contains the backend and frontend poject implementations of Partner Demo application.

## Requirements

Tested with Node version `16.13.1` and NPM version `8.1.2`.

## Usage

Run server with the following:
```shell
node src/app.js
```
After successful startup the server is listening on `http://localhost:3001`.

Run client with the following:
```shell
npm start
```
After successful startup the application can be accessed on `http://localhost:3000`.