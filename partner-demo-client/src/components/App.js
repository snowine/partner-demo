import React, {useState} from 'react';
import {PartnerList} from "./partners/PartnerList";
import {Container} from "@mui/material";
import {PartnerDialog} from "./partners/PartnerDialog";
import {MasterDataDialog} from "./masterData/MasterDataDialog";

function App() {
    const [isAddOpen, setAddOpen] = useState(false);
    const [isSettingsOpen, setSettingsOpen] = useState(false);

    return (
        <Container maxWidth={"lg"}>
            <PartnerList
                onAdd={() => setAddOpen(true)}
                onSettings={() => setSettingsOpen(true)}
            />
            <PartnerDialog
                open={isAddOpen}
                onClose={() => {
                    setAddOpen(false);

                }}/>
            <MasterDataDialog
                open={isSettingsOpen}
                onClose={() => {
                    setSettingsOpen(false);
                }}/>
        </Container>
    );
}

export default App;
