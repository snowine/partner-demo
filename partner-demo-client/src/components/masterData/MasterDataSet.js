import React from "react";
import {Box, Typography} from "@mui/material";
import {MasterDataEntry} from "./MasterDataEntry";
import {MasterDataCreator} from "./MasterDataCreator";

export function MasterDataSet({label, entities, onCreate, onUpdate, onDelete}) {
    return (
        <>
            <Typography variant={"overline"}>{label}</Typography>
            <Box display={"flex"} flexWrap={"wrap"}>
                {entities.map((entity, index) =>
                    <Box key={"master-data-entry-" + index} padding={0.6}>
                        <MasterDataEntry
                            name={entity.name}
                            onUpdate={name => onUpdate(entity.id, name, entity.version)}
                            onDelete={() => onDelete(entity.id, entity.version)}
                        />
                    </Box>
                )}
                <Box padding={0.6}>
                    <MasterDataCreator onCreate={name => onCreate(name)}/>
                </Box>
            </Box>
        </>
    );
}
