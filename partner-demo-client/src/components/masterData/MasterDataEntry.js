import React, {useState} from "react";
import {Box, Paper, TextField, Typography} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import EditIcon from '@mui/icons-material/Edit';
import ClearIcon from '@mui/icons-material/Clear';
import DoneIcon from '@mui/icons-material/Done';
import {MasterDataAction} from "./MasterDataAction";

export function MasterDataEntry({name, onDelete, onUpdate}) {
    const [editedValue, setEditedValue] = useState(null);

    return (
        <Paper>
            <Box display={"flex"} flexDirection={"row"} p={1}>
                <Box px={editedValue ? 0 : 3}>
                    {editedValue ?
                        <TextField id="text" variant="standard" value={editedValue} onChange={event => setEditedValue(event.target.value)}/> :
                        <Typography display={"inline"} variant={"subtitle1"}>{name}</Typography>
                    }
                </Box>
                {editedValue ?
                    <Box>
                        <MasterDataAction onEvent={() => setEditedValue(null)}>
                            <ClearIcon fontSize={"inherit"}/>
                        </MasterDataAction>
                        <MasterDataAction onEvent={() => {
                            onUpdate(editedValue);
                            setEditedValue(null)
                        }}>
                            <DoneIcon fontSize={"inherit"}/>
                        </MasterDataAction>
                    </Box> :
                    <Box>
                        <MasterDataAction onEvent={() => setEditedValue(name)}>
                            <EditIcon fontSize={"inherit"}/>
                        </MasterDataAction>
                        <MasterDataAction onEvent={onDelete}>
                            <DeleteIcon fontSize={"inherit"}/>
                        </MasterDataAction>
                    </Box>
                }
            </Box>
        </Paper>
    );
}
