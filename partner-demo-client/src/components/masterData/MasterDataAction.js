import React from "react";
import {IconButton} from "@mui/material";

export function MasterDataAction({children, onEvent}) {
    return (
        <IconButton size={"small"} color={"default"} onClick={onEvent}>
            {children}
        </IconButton>
    );
}
