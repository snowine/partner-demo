import React, {useState} from "react";
import {Box, Paper, TextField} from "@mui/material";
import {MasterDataAction} from "./MasterDataAction";
import AddIcon from '@mui/icons-material/Add';
import ClearIcon from '@mui/icons-material/Clear';
import DoneIcon from '@mui/icons-material/Done';

export function MasterDataCreator({onCreate}) {
    const [editing, setEditing] = useState(false);
    const [name, setName] = useState("");

    return (
        <Paper>
            {editing ?
                <Box p={1}>
                    <TextField variant={"standard"} value={name} onChange={event => setName(event.target.value)} />
                    <MasterDataAction onEvent={() => {
                        setName("");
                        setEditing(false);
                    }}>
                        <ClearIcon fontSize={"inherit"}/>
                    </MasterDataAction>
                    <MasterDataAction onEvent={() => {
                        onCreate(name);
                        setName(null);
                        setEditing(false);
                    }}>
                        <DoneIcon fontSize={"inherit"}/>
                    </MasterDataAction>
                </Box> :
                <Box p={1}>
                    <MasterDataAction onEvent={() => setEditing(true)}>
                        <AddIcon fontSize={"inherit"}/>
                    </MasterDataAction>
                </Box>
            }
        </Paper>
    );
}
