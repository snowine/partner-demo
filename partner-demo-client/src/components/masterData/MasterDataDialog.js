import React, {useEffect} from "react";
import {Box, Dialog, DialogContent, DialogTitle, IconButton, Typography} from "@mui/material";
import CloseIcon from '@mui/icons-material/Close';
import {useDispatch, useSelector} from "react-redux";
import {fetchCities} from "../../state/citySlice";
import {fetchTypes} from "../../state/typeSlice";
import {createCity, updateCity, deleteCity} from "../../client/cityClient"
import {createType, updateType, deleteType} from "../../client/typeClient"
import {MasterDataSet} from "./MasterDataSet";
import {useNotifications} from "../../utils/notification";
import {fetchPartners} from "../../state/partnerSlice";

export function MasterDataDialog({open, onClose, id}) {
    const {enqueueSuccess, enqueueError} = useNotifications();
    const dispatch = useDispatch();
    const {entities: cities} = useSelector(state => state.cities);
    const {entities: types} = useSelector(state => state.types);

    useEffect(() => {
        if (open) {
            dispatch(fetchCities());
            dispatch(fetchTypes());
        }
    }, [dispatch, open]);

    return (
        <Dialog open={open} onClose={onClose} maxWidth={"md"} fullWidth>
            <DialogTitle>
                <Box display={"flex"} flexDirection={"row"} justifyContent={"space-between"}>
                    <Typography>Master Data Management</Typography>
                    <IconButton onClick={() => onClose()}><CloseIcon/></IconButton>
                </Box>
            </DialogTitle>
            <DialogContent>
                <Box py={1}>
                    <MasterDataSet
                        label={"Cities"}
                        entities={cities}
                        onCreate={name =>
                            createCity(name)
                                .then(() => {
                                    dispatch(fetchCities());
                                    enqueueSuccess();
                                })
                                .catch(reason => enqueueError(reason))}
                        onUpdate={(id, name, version) =>
                            updateCity(id, name, version)
                                .then(() => {
                                    dispatch(fetchCities());
                                    dispatch(fetchPartners());
                                    enqueueSuccess();
                                })
                                .catch(reason => enqueueError(reason))}
                        onDelete={(id, version) =>
                            deleteCity(id, version)
                                .then(() => {
                                    dispatch(fetchCities());
                                    enqueueSuccess();
                                })
                                .catch(reason => enqueueError())}
                    />
                </Box>
                <Box py={1}>
                    <MasterDataSet
                        label={"Business Forms"}
                        entities={types}
                        onCreate={name =>
                            createType(name)
                                .then(() => {
                                    dispatch(fetchTypes());
                                    enqueueSuccess();
                                })
                                .catch(reason => enqueueError(reason))}
                        onUpdate={(id, name, version) =>
                            updateType(id, name, version)
                                .then(() => {
                                    dispatch(fetchTypes());
                                    dispatch(fetchPartners());
                                    enqueueSuccess();
                                })
                                .catch(reason => enqueueError(reason))}
                        onDelete={(id, version) =>
                            deleteType(id, version)
                                .then(() => {
                                    dispatch(fetchTypes());
                                    enqueueSuccess();
                                })
                                .catch(reason => enqueueError(reason))}
                    />
                </Box>
            </DialogContent>
        </Dialog>
    );
}
