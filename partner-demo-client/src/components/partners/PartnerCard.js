import React, {useState} from "react";
import {Box, Chip, Collapse, Divider, Grid, IconButton, Typography} from "@mui/material";
import EditIcon from '@mui/icons-material/Edit';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import {PartnerDialog} from "./PartnerDialog";

function Attribute({label, value}) {
    return <Grid item xs={4}>
        <Typography display={"block"} variant={"overline"}>{label}</Typography>
        <Typography display={"block"} variant={"subtitle1"}>{value || "-"}</Typography>
    </Grid>;
}

const attributeDefs = [
    {label: "Tax number", attributeName: "tax_number"},
    {label: "Registration number", attributeName: "registration_number"},
    {label: "Address", attributeName: "address"},
    {label: "Phone", attributeName: "phone"},
    {label: "Bank account no.", attributeName: "bank_account_no"},
    {label: "Note", attributeName: "note"}
];

export function PartnerCard({partner, open, onViewChange}) {
    const [editDialogOpen, setEditDialogOpen] = useState(false);

    return (
        <>
            <PartnerDialog
                id={partner.id}
                open={editDialogOpen}
                onClose={() => setEditDialogOpen(false)}
            />
            <Box sx={{display: "flex", justifyContent: "flex-start"}}>
                <Box>
                    <IconButton onClick={onViewChange}>
                        {open ? <KeyboardArrowDownIcon/> : <KeyboardArrowRightIcon/>}
                    </IconButton>
                </Box>
                <Box sx={{flexDirection: "column"}}>
                    <Typography display={"block"} variant={"button"}>{partner.name}</Typography>
                    <Typography display={"block"} variant={"caption"}>{partner.city}</Typography>
                </Box>
                <Box flexGrow={1} px={1}>
                    <Chip variant="outlined" color="primary" label={partner.type} size={"small"}/>
                </Box>
                <Box>
                    <IconButton size={"small"} onClick={() => setEditDialogOpen(true)}>
                        <EditIcon fontSize={"inherit"}/>
                    </IconButton>
                </Box>
            </Box>
            <Collapse in={open}>
                <Divider/>
                <Box padding={1}>
                    <Grid container spacing={1}>
                        {attributeDefs.map((def, index) => <Attribute key={"attribute-" + index} label={def.label} value={partner[def.attributeName]}/>)}
                    </Grid>
                </Box>
            </Collapse>
        </>
    );
}
