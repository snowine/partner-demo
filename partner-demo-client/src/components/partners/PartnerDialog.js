import {
    Box,
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Fade,
    FormControl,
    Grid,
    IconButton,
    InputLabel,
    MenuItem,
    Select,
    TextField
} from "@mui/material";
import AddIcon from '@mui/icons-material/Add';
import ClearIcon from '@mui/icons-material/Clear';
import DoneIcon from '@mui/icons-material/Done';
import React, {useCallback, useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchCities} from "../../state/citySlice";
import {createPartner, getPartner, updatePartner} from "../../client/partnerClient";
import {fetchTypes} from "../../state/typeSlice";
import {useNotifications} from "../../utils/notification";
import {createType} from "../../client/typeClient";
import {createCity} from "../../client/cityClient";
import {fetchPartners} from "../../state/partnerSlice";

const emptyFormData = {name: "", type: "", city: "", tax_number: "", registration_number: "", address: "", phone: "", bank_account_no: "", note: ""};

const attributeDefs = [
    {label: "Tax number", attribute: "tax_number"},
    {label: "Registration number", attribute: "registration_number"},
    {label: "Address", attribute: "address"},
    {label: "Phone", attribute: "phone"},
    {label: "Bank account no", attribute: "bank_account_no"},
    {label: "Note", attribute: "note"}
];

function CreatableSelect({selectedValue, onSelectedValueChange, inCreation, onCreationChange, createFunction, selectorFunction}) {
    const [editedValue, setEditedValue] = useState("");
    const {entities: types} = useSelector(selectorFunction);

    return (
        <>
            {inCreation ?
                    <Box display={"flex"} flexDirection={"row"} alignContent={"center"} flexGrow={1}>
                        <TextField
                            sx={{minWidth: "50%"}}
                            variant={"standard"}
                            label={"New type"}
                            value={editedValue}
                            onChange={event => setEditedValue(event.target.value)}/>
                        <IconButton onClick={() => {
                            onCreationChange(false);
                            setEditedValue("")
                        }}>
                            <ClearIcon/>
                        </IconButton>
                        <IconButton onClick={() => {
                            createFunction(editedValue)
                                .then(() => setEditedValue(""))
                        }}>
                            <DoneIcon/>
                        </IconButton>
                    </Box> :
                    <Box display={"flex"} flexDirection={"row"} alignContent={"center"} flexGrow={1}>
                        <FormControl variant={"standard"} sx={{minWidth: "50%"}}>
                            <InputLabel>Type</InputLabel>
                            <Select
                                value={selectedValue}
                                onChange={event => onSelectedValueChange(event.target.value)}
                            >
                                {types.map((type, index) => <MenuItem key={"type-" + index} value={type.id}>{type.name}</MenuItem>)}
                            </Select>
                        </FormControl>
                        <IconButton onClick={() => onCreationChange(true)}>
                            <AddIcon/>
                        </IconButton>
                    </Box>}
        </>
    );
}

export function PartnerDialog({open, onClose, id}) {
    const {enqueueSuccess, enqueueError} = useNotifications();
    const dispatch = useDispatch();
    const [formData, setFormData] = useState(null)
    const [createdMasterData, setCreatedMasterData] = useState(null);

    const handleClose = useCallback(() => {
        onClose();
        setFormData(null);
    }, [setFormData, onClose]);

    const persist = useCallback((onSuccess) => {
        const promise = id ? updatePartner(id, formData) : createPartner(formData);
        promise.then(() => {enqueueSuccess(); dispatch(fetchPartners()); onSuccess();})
                .catch(reason => enqueueError(reason));
    }, [formData, id, enqueueSuccess, enqueueError, dispatch])

    useEffect(() => {
        if (open) {
            if (id)
                getPartner(id).then(data => setFormData(data));
            else
                setFormData(emptyFormData);
        }
    }, [id, open])

    useEffect(() => {
        dispatch(fetchTypes());
        dispatch(fetchCities());
    }, [dispatch])

    return (
        <Dialog open={open} maxWidth={"md"}>
            <DialogTitle>{id ?  "Edit partner" : "Create partner"}</DialogTitle>
            <DialogContent>
                <Fade in={!!formData}>
                    <Grid container spacing={2} padding={1}>
                        <Grid item xs={12}>
                            <TextField
                                value={formData?.name || ""}
                                onChange={event => setFormData(prevState => ({...prevState, name: event.target.value}))}
                                label={"Name"}
                                variant={"standard"}
                                fullWidth/>
                        </Grid>
                        <Grid item xs={12}>
                            <CreatableSelect
                                inCreation={createdMasterData === "type"}
                                onCreationChange={value => value ? setCreatedMasterData("type") : setCreatedMasterData(null)}
                                selectedValue={formData?.type || ""}
                                onSelectedValueChange={id => setFormData(prevState => ({...prevState, type: id}))}
                                selectorFunction={state => state.types}
                                createFunction={name =>
                                    createType(name)
                                        .then(id => {
                                            dispatch(fetchTypes())
                                                .then(() => setFormData(prevState => ({...prevState, type: id})));
                                            setCreatedMasterData(null);
                                            enqueueSuccess();
                                        })
                                        .catch(reason => enqueueError(reason))}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <CreatableSelect
                                inCreation={createdMasterData === "city"}
                                onCreationChange={value => value ? setCreatedMasterData("city") : setCreatedMasterData(null)}
                                selectedValue={formData?.city || ""}
                                onSelectedValueChange={id => setFormData(prevState => ({...prevState, city: id}))}
                                selectorFunction={state => state.cities}
                                createFunction={name =>
                                    createCity(name)
                                        .then(id => {
                                            dispatch(fetchCities())
                                                .then(() => setFormData(prevState => ({...prevState, city: id})));
                                            setCreatedMasterData(null);
                                            enqueueSuccess();
                                        })
                                        .catch(reason => enqueueError(reason))}
                            />
                        </Grid>
                        {attributeDefs.map(def => {
                            return (
                                <Grid key={def.attribute} item xs={6}>
                                    <TextField
                                        value={formData ? formData[def.attribute] || "" : ""}
                                        onChange={event => setFormData(prevState => ({...prevState, [def.attribute]: event.target.value}))}
                                        label={def.label}
                                        variant={"standard"}
                                        fullWidth/>
                                </Grid>
                            )
                        })}
                    </Grid>
                </Fade>
            </DialogContent>
            <DialogActions>
                <Button onClick={() => {handleClose(); setCreatedMasterData(null)}}>Cancel</Button>
                <Button disabled={createdMasterData !== null} onClick={() => persist(handleClose)}>{id ? "Save" : "Create"}</Button>
            </DialogActions>
        </Dialog>
    );
}
