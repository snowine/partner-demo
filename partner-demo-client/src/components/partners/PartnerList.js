import React, {useCallback, useEffect, useMemo, useState} from 'react';
import {Box, CircularProgress, IconButton, Pagination, Paper, Stack, Tooltip, Typography} from "@mui/material";
import SettingsIcon from '@mui/icons-material/Settings';
import AddIcon from '@mui/icons-material/Add';
import {useDispatch, useSelector} from "react-redux";
import {fetchPartners, PAGE_SIZE, setPage} from "../../state/partnerSlice";
import {PartnerListFilter} from "./PartnerListFilter";
import {PartnerCard} from "./PartnerCard";

function ListHeader({total, onAdd, onSettings}) {
    return (
        <Paper variant={"elevation"}>
            <Box display={"flex"} paddingY={1} paddingX={2} alignItems={"flex-end"} justifyItems={"space-between"} flexDirection={"row"}>
                <Box px={1}>
                    <PartnerListFilter/>
                </Box>
                <Box flexGrow={1}>
                    <Typography variant={"caption"}>Total: {total + (total === 1 ? " partner" : " partners")}</Typography>
                </Box>
                <Box>
                    <Tooltip title={"Add new partner"} arrow={true}>
                        <IconButton onClick={onAdd}>
                            <AddIcon/>
                        </IconButton>
                    </Tooltip>
                    <Tooltip title={"Settings"} arrow={true}>
                        <IconButton onClick={onSettings}>
                            <SettingsIcon/>
                        </IconButton>
                    </Tooltip>
                </Box>
            </Box>
        </Paper>
    );
}

export function PartnerList({onAdd, onSettings}) {
    const dispatch = useDispatch();
    const {result, status, page} = useSelector(state => state.partners);
    const [openIndex, setOpenIndex] = useState(null);

    const totalPages = useMemo(() => Math.ceil((result ? result.total : 0) / PAGE_SIZE), [result]);
    const handlePageChange = useCallback((event, page) => {
        dispatch(setPage(page));
        dispatch(fetchPartners());
    }, [dispatch]);

    useEffect(() => {
        dispatch(fetchPartners());
    }, [dispatch])

    return (
        <Box p={1}>
            <Stack spacing={1}>
                <Box paddingY={1}>
                    <ListHeader total={result?.total | 0} onAdd={onAdd} onSettings={onSettings}/>
                </Box>
                {result?.results.map((partner, index) => {
                    return (
                        <Paper key={"partner-element-" + index} variant={"outlined"}>
                            <Box paddingY={1} paddingX={2}>
                                <PartnerCard
                                    partner={partner}
                                    open={index === openIndex}
                                    onViewChange={index === openIndex ?
                                        () => setOpenIndex(null) :
                                        () => setOpenIndex(index)}/>
                            </Box>
                        </Paper>
                    );
                })}
                {status !== "idle" ?
                    <div align={"center"}><CircularProgress size={20} color={"info"} variant={"indeterminate"} disableShrink/></div> :
                    <Pagination style={{margin: "auto", paddingTop: "10px"}} page={page} count={totalPages} sx={{flexShrink: 1}} onChange={handlePageChange}/>
                }
            </Stack>
        </Box>
    );
}
