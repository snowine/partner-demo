import React, {useEffect, useMemo, useState} from 'react';
import {Box, FormControl, InputLabel, MenuItem, Select, TextField} from "@mui/material";
import debounce from 'lodash/debounce';
import {useDispatch, useSelector} from "react-redux";
import {fetchTypes} from "../../state/typeSlice"
import {fetchPartners, setNameFilter, setTypeFilter} from "../../state/partnerSlice";

export function PartnerListFilter() {
    const dispatch = useDispatch();
    const {entities: types} = useSelector(state => state.types);
    const {name: globalNameFilter, type: typeFilter} = useSelector(state => state.partners.filter);
    const [localNameFilter, setLocalNameFilter] = useState("");

    const debounceNameChange = useMemo(() => {
        return debounce(name => {
            dispatch(setNameFilter(name));
            dispatch(fetchPartners());
        }, 300)
    }, [dispatch]);

    useEffect(() => {
        debounceNameChange(localNameFilter);
    }, [localNameFilter, debounceNameChange])

    useEffect(() => {
        setLocalNameFilter(globalNameFilter || "");
    }, [globalNameFilter, setLocalNameFilter])

    useEffect(() => {
        dispatch(fetchTypes());
    }, [dispatch])

    return (
        <Box display={"flex"} flexDirection={"row"}>
            <Box px={2}>
                <TextField
                    variant={"standard"}
                    size={"small"}
                    label={"Filter by name"}
                    value={localNameFilter}
                    onChange={event => setLocalNameFilter(event.target.value)}/>
            </Box>
            <Box px={2}>
                <FormControl variant={"standard"} size={"small"} sx={{minWidth: 180}}>
                    <InputLabel>Filter by type</InputLabel>
                    <Select value={typeFilter || ""} onChange={event => {
                        dispatch(setTypeFilter(event.target.value));
                        dispatch(fetchPartners());
                    }}>
                        <MenuItem value={""}><em>Any</em></MenuItem>
                        {types.map(type => <MenuItem key={type.id} value={type.id}>{type.name}</MenuItem>)}
                    </Select>
                </FormControl>
            </Box>
        </Box>
    );
}
