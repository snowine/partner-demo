import { configureStore } from "@reduxjs/toolkit"
import {partnersSlice} from "./partnerSlice"
import {citiesSlice} from "./citySlice"
import {typesSlice} from "./typeSlice"

export default configureStore({
    reducer: {
        [partnersSlice.name]: partnersSlice.reducer,
        [citiesSlice.name]: citiesSlice.reducer,
        [typesSlice.name]: typesSlice.reducer
    }
})
