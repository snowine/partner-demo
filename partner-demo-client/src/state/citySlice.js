import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {httpClient} from "../client/http/httpClient";

const initialState = {
    entities: [],
    loading: false
}

export const fetchCities = createAsyncThunk(
    "cities/getCities",
    async (args, thunkAPI) => {
        return await httpClient.get(`/cities`)
            .then(res => {
                if (res.status === 200) {
                    return res.data;
                } else
                    throw new Error("Fetching cities resulted in status " + res.status);
            });
    }
)

export const citiesSlice = createSlice({
    name: "cities",
    initialState,
    reducers: {},
    extraReducers: {
        [fetchCities.pending]: (state) => {
            state.loading = true
        },
        [fetchCities.fulfilled]: (state, {payload}) => {
            state.entities = payload
            state.loading = false
        },
        [fetchCities.rejected]: (state) => {
            state.loading = false
        }
    }
});
