import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {httpClient} from "../client/http/httpClient";

const initialState = {
    entities: [],
    loading: false
}

export const fetchTypes = createAsyncThunk(
    "types/getTypes",
    async (args, thunkAPI) => {
        return await httpClient.get(`/types`)
            .then(res => {
                if (res.status === 200) {
                    return res.data;
                } else
                    throw new Error("Fetching cities resulted in status " + res.status);
            });
    }
)

export const typesSlice = createSlice({
    name: "types",
    initialState,
    reducers: {},
    extraReducers: {
        [fetchTypes.pending]: (state) => {
            state.loading = true
        },
        [fetchTypes.fulfilled]: (state, {payload}) => {
            state.entities = payload
            state.loading = false
        },
        [fetchTypes.rejected]: (state) => {
            state.loading = false
        }
    }
});
