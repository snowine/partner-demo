import {createAsyncThunk, createSlice} from "@reduxjs/toolkit";
import {httpClient} from "../client/http/httpClient";

export const PAGE_SIZE = 5;

const initialState = {
    result: null,
    state: "idle",
    page: 1,
    filter: {
        name: null,
        type: null
    }
}

export const fetchPartners = createAsyncThunk(
    "partners/getPartners",
    async (args, thunkAPI) => {
        const {page, filter} = thunkAPI.getState().partners;
        return await httpClient.get(`/partners?offset=${(page-1)*PAGE_SIZE}&limit=${PAGE_SIZE}` +
                                                        `${filter.name ? `&name=${filter.name}` : ""}` +
                                                        `${filter.type ? `&type=${filter.type}` : ""}`)
            .then(res => {
                if (res.status === 200) {
                    return res.data;
                } else
                    throw new Error("Fetching partners resulted in status " + res.status);
            });
    }
)

export const partnersSlice = createSlice({
    name: "partners",
    initialState,
    reducers: {
        setPage: (state, {payload}) => {
            state.page = payload
        },
        setNameFilter: (state, {payload}) => {
            state.filter.name = payload
            state.page = 1
        },
        setTypeFilter: (state, {payload}) => {
            state.filter.type = payload
            state.page = 1
        }
    },
    extraReducers: {
        [fetchPartners.pending]: (state) => {
            state.status = "loading";
        },
        [fetchPartners.fulfilled]: (state, {payload}) => {
            state.result = payload;
            state.status = "idle";
        },
        [fetchPartners.rejected]: (state) => {
            state.status = "idle";
        }
    }
});

export const {setNameFilter, setTypeFilter, setPage} = partnersSlice.actions;
