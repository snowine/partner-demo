import {useSnackbar} from "notistack";
import {useMemo} from "react";

const defaultSnackbarOptions = {
    anchorOrigin: {horizontal: "center", vertical: "bottom"},
    autoHideDuration: 3000
}

export const useNotifications = () => {
    const {enqueueSnackbar} = useSnackbar();
    return useMemo(() => (
        {
            enqueueSuccess: message =>
                enqueueSnackbar(
                    "Operation succeeded" + (message ? `: ${message}` : ""),
                    {...defaultSnackbarOptions, variant: "success"}),
            enqueueError: message =>
                enqueueSnackbar(
                    "Operation failed" + (message ? `: ${message}` : ""),
                    {...defaultSnackbarOptions, variant: "error"})
        }
    ), [enqueueSnackbar]);
}
