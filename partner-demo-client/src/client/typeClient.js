import {checkStatus, httpClient} from "./http/httpClient";

export const createType =  async (name) => {
    return await httpClient.post("/types", {name: name})
        .then(checkStatus)
        .then(res => res.data.id);
}

export const deleteType = async (id, version) => {
    return await httpClient.delete(`/types/${id}`, {data: {version: version}})
        .then(checkStatus);
}

export const updateType = async (id, name, version) => {
    return await httpClient.put(`/types/${id}`, {name: name, version: version})
        .then(checkStatus);
}
