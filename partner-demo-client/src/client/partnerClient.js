import {checkStatus, httpClient} from "./http/httpClient";

export const getPartner = async id => {
    return await httpClient.get(`/partners/${id}`)
        .then(checkStatus)
        .then(res => res.data)
}

export const createPartner = async data => {
    return await httpClient.post("/partners", data)
        .then(checkStatus)
        .then(res => res.data.id)
}

export const updatePartner = async (id, data) => {
    return await httpClient.put(`/partners/${id}`, data)
        .then(checkStatus)
}

export const deletePartner = async (id, version) => {
    return await httpClient.delete(`/partners/${id}`, {data: {version: version}})
}
