import {checkStatus, httpClient} from "./http/httpClient";

export const createCity =  async (name) => {
    return await httpClient.post("/cities", {name: name})
        .then(checkStatus)
        .then(res => res.data.id);
}

export const updateCity = async (id, name, version) => {
    return await httpClient.put(`/cities/${id}`, {name: name, version: version})
        .then(checkStatus);
}

export const deleteCity = async (id, version) => {
    return await httpClient.delete(`/cities/${id}`, {data: {version: version}})
        .then(checkStatus);
}
