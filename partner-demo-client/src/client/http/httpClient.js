import axios from "axios";

export const httpClient = axios.create({baseURL: "http://localhost:3001"});

export const checkStatus = res => {
    if (res.status !== 200)
        throw new Error(res.status);
    else
        return res;
}
