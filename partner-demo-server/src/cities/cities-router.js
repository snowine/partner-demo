const router = require('express').Router();
const {body, param} = require("express-validator");
const validationFilter = require("../validation-filter-middleware");

module.exports = cityRepository => {
    router.get("/",async (req, res) => {
        res.json(await cityRepository.get());
    });

    router.post("/",
        body("name").isString().notEmpty(),
        validationFilter(),
        (req, res) => {
            cityRepository.create({
                $name: req.body.name
            })
                .then(value => res.json({id: value}))
                .catch(reason => {
                    res.status(500).send(reason);
                })
        });

    router.put("/:id",
        param("id").isNumeric(),
        body("name").isString().notEmpty(),
        body("version").isNumeric(),
        validationFilter(),
        (req, res) => {
        cityRepository.update({
            $id: parseInt(req.params.id),
            $name: req.body.name,
            $version: req.body.version
        })
                .then(() => res.sendStatus(200))
                .catch(reason => {
                    res.status(400).send(reason);
                })
        })

    router.delete("/:id",
        param("id").isNumeric(),
        body("version").isNumeric(),
        validationFilter(),
        (req, res) => {
        cityRepository.delete({
            $id: parseInt(req.params.id),
            $version: req.body.version
        })
            .then(() => res.sendStatus(200))
            .catch(reason => res.status(400).send(reason))
    })

    return router;
}
