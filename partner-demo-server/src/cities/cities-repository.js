const {select, insert, updateOrDelete} = require("../utils/sqlite-utils");

class CitiesRepository {
    constructor(sqliteDb) {
        this.db = sqliteDb;
    }

    get() {
        return select("SELECT rowid AS id, name, version FROM city", this.db);
    }

    create(city) {
        return insert("INSERT INTO city (name, version) VALUES ($name, 0)", city, this.db);
    }

    update(city) {
        return updateOrDelete("UPDATE city SET name = $name, version = version + 1 WHERE rowid = $id AND version = $version", city, this.db);
    }

    delete(city) {
        return updateOrDelete("DELETE FROM city WHERE rowid = $id AND version = $version", city, this.db);
    }
}

module.exports = CitiesRepository;
