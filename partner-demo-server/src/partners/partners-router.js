const router = require('express').Router();
const {query, param, body} = require("express-validator");
const validationFilter = require("../validation-filter-middleware");

module.exports = partnersRepository => {
    router.get("/",
        query("offset").isInt().custom(input => input >= 0),
        query("limit").isInt().custom(input => input > 0),
        query("name").optional().isString().notEmpty(),
        query(["city", "type"]).optional().isInt(),
        validationFilter(),
        (req, res) => {
            let page = {$offset: req.query.offset, $limit: req.query.limit}
            let filter = {};
            if (req.query.name)
                filter = {$name: req.query.name};
            if (req.query.type)
                filter = {$type: req.query.type, ...filter};
            if (req.query.city)
                filter = {$city: req.query.city, ...filter};
            let total = partnersRepository.count(filter);
            let results = partnersRepository.search({...page, ...filter});
            Promise.all([total, results])
                .then(([total, results]) => res.json({total: total, results: results}))
                .catch(reason => res.status(500).send(reason));
        });

    router.post("/",
        body("name").isString().notEmpty(),
        body(["type", "city"]).isInt(),
        body(["tax_number", "registration_number", "address", "phone", "bank_account_no", "note"])
            .optional({nullable: true}).isString(),
        validationFilter(),
        (req, res) => {
            partnersRepository.create({
                $name: req.body.name,
                $type: req.body.type,
                $city: req.body.city,
                $tax_number: req.body.tax_number,
                $registration_number: req.body.registration_number,
                $address: req.body.address,
                $phone: req.body.phone,
                $bank_account_no: req.body.bank_account_no,
                $note: req.body.note
            })
                .then(value => res.json({id: value}))
                .catch(reason => res.status(500).send(reason))
        });

    router.get("/:id",
        param("id").isInt(),
        validationFilter(),
        (req, res) => {
            partnersRepository.get({$id: req.params.id})
                .then(value => value ? res.json(value) : res.status(404).send())
                .catch(reason => res.status(500).send(reason))
        });

    router.put("/:id",
        param("id").isInt(),
        body("name").isString().notEmpty(),
        body(["city", "type", "version"]).isInt(),
        body(["tax_number", "registration_number", "address", "phone", "bank_account_no", "note"])
            .optional({nullable: true}).isString(),
        validationFilter(),
        (req, res) => {
            partnersRepository.update({
                $id: req.params.id,
                $name: req.body.name,
                $city: req.body.city,
                $type: req.body.type,
                $tax_number: req.body.tax_number,
                $registration_number: req.body.registration_number,
                $address: req.body.address,
                $phone: req.body.phone,
                $bank_account_no: req.body.bank_account_no,
                $note: req.body.note,
                $version: req.body.version
            })
                .then(() => res.sendStatus(200))
                .catch(reason => res.status(500).send(reason));
        })

    router.delete("/:id",
        param("id").isInt(),
        body("version").isInt(),
        validationFilter(),
        (req, res) => {
            partnersRepository.delete({
                $id: req.params.id,
                $version: req.body.version
            })
                .then(() => res.sendStatus(200))
                .catch(reason => res.status(500).send(reason));
        })

    return router;
}
