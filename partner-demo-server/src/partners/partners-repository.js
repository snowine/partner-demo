const {select, insert, updateOrDelete} = require("../utils/sqlite-utils");

function getFilters(options) {
    const filters = [];
    if (options.$name) {
        filters.push("LOWER(partner.name) LIKE LOWER($name)");
        options.$name = `%${options.$name}%`
    }
    if (options.$type)
        filters.push("partner.business_form = $type")
    if (options.$city)
        filters.push("partner.city = $city")
    return filters;
}

function getWhereClause(filters) {
    if (filters.length > 0) {
        let whereClause = `WHERE ${filters[0]}`;
        filters.slice(1).forEach(filter => {
            whereClause = whereClause.concat(` AND ${filter}`)
        });
        return whereClause;

    }
    else
        return null;
}

class PartnersRepository {
    constructor(sqliteDb) {
        this.db = sqliteDb;
    }

    count(options) {
        return new Promise((resolve, reject) => {
            this.db.get(`SELECT count(rowid) AS total FROM partner ${getWhereClause(getFilters(options)) || ""}`, options, function (err, row) {
                if (err)
                    reject(err.message);
                else
                    resolve(row.total);
            });
        })
    }

    search(options) {
        return select(`
                        SELECT partner.rowid AS id,
                               partner.name,
                               city.name AS city,
                               business_form.name AS type,
                               partner.tax_number,
                               partner.registration_number,
                               partner.address,
                               partner.phone,
                               partner.bank_account_no,
                               partner.note,
                               partner.version
                        FROM partner INNER JOIN city ON partner.city=city.rowid
                                    INNER JOIN business_form ON partner.business_form = business_form.rowid
                        ${getWhereClause(getFilters(options)) || ""}
                        ORDER BY partner.name LIMIT $limit OFFSET $offset`, this.db, options)
    }

    get(partner) {
        return select(`
                             SELECT rowid AS id,
                                    name,
                                    city,
                                    business_form AS type,
                                    tax_number,
                                    registration_number,
                                    address,
                                    phone,
                                    bank_account_no,
                                    note,
                                    version
                             FROM partner
                             WHERE rowid = $id`, this.db, partner)
            .then(rows => rows ? rows[0] : null);
    }

    create(partner) {
        return insert(`INSERT INTO partner (name, business_form, city, tax_number, registration_number,
                                            address, phone, bank_account_no, note, version)
                                VALUES ($name, $type, $city, $tax_number, $registration_number,
                                            $address, $phone, $bank_account_no, $note, 0)`,
            partner,
            this.db);
    }

    update(partner) {
        return updateOrDelete(`
                                    UPDATE partner
                                    SET name=$name,
                                        city=$city,
                                        business_form=$type,
                                        tax_number=$tax_number,
                                        registration_number=$registration_number,
                                        address=$address,
                                        phone=$phone,
                                        bank_account_no=$bank_account_no,
                                        note=$note,
                                        version = version + 1
                                    WHERE rowid=$id AND version=$version`,
            partner,
            this.db);
    }

    delete(partner) {
        return updateOrDelete("DELETE FROM partner WHERE rowid=$id AND version=$version",
            partner,
            this.db);
    }
}

module.exports = PartnersRepository;
