const {body, param} = require("express-validator");
const validationFilter = require("../validation-filter-middleware");
module.exports = function (typesRepository) {
    const router = require("express").Router();

    router.get("/", async (req, res) => {
        res.send(await typesRepository.get());
    });

    router.post("/",
        body("name").isString().notEmpty(),
        validationFilter(),
        (req, res) => {
        typesRepository.create({
            $name: req.body.name
        })
            .then(value => res.json({id: value}))
            .catch(reason => res.status(500).send(reason));
    })

    router.put("/:id",
        param("id").isNumeric(),
        body("name").isString().notEmpty(),
        body("version").isNumeric(),
        validationFilter(),
        (req, res) => {
        typesRepository.update({
            $id: req.params.id,
            $name: req.body.name,
            $version: req.body.version
        })
            .then(() => res.sendStatus(200))
            .catch(reason => res.status(500).send(reason));
    })

    router.delete("/:id",
        param("id").isNumeric(),
        body("version").isNumeric(),
        validationFilter(),
        (req, res) => {
        typesRepository.delete({
            $id: req.params.id,
            $version: req.body.version
        })
            .then(() => res.sendStatus(200))
            .catch(reason => res.status(500).send(reason));
    })

    return router;
}
