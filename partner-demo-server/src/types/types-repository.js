const {select, insert, updateOrDelete} = require("../utils/sqlite-utils");

class TypesRepository {
    constructor(sqliteDb) {
        this.db = sqliteDb;
    }

    get() {
        return select("SELECT rowid AS id, name, version FROM business_form", this.db);
    }

    create(type) {
        return insert("INSERT INTO business_form (name, version) VALUES ($name, 0)", type, this.db);
    }

    update(type) {
        return updateOrDelete("UPDATE business_form SET name=$name, version = version + 1 WHERE rowid=$id AND version=$version", type, this.db);
    }

    delete(type) {
        return updateOrDelete("DELETE FROM business_form WHERE rowid=$id AND version=$version", type, this.db);
    }
}

module.exports = TypesRepository;
