const sqlite3 = require('sqlite3').verbose();
const initialize = require("./schema");
const {populateCities, populateTypes, populatePartners} = require("./test-data");
const CitiesRepository = require('./cities/cities-repository');
const TypesRepository = require('./types/types-repository');
const PartnersRepository = require("./partners/partners-repository");

const db = new sqlite3.Database(':memory:');
initialize(db);

const citiesRepository = new CitiesRepository(db);
const typesRepository = new TypesRepository(db);
const partnersRepository = new PartnersRepository(db);
Promise.all([
    populateCities(citiesRepository),
    populateTypes(typesRepository)
])
    .then(() => populatePartners(partnersRepository));

const citiesRouter = require('./cities/cities-router')(citiesRepository);
const typesRouter = require("./types/types-router")(typesRepository);
const partnersRouter = require("./partners/partners-router")(partnersRepository);

const express = require("express");
const cors = require("cors");
const {loggingMiddleware, createLogger} = require("./logging");
const port = process.env.PORT || 3001;
const app = express();
app.use(cors());
app.use(express.json());
app.use(loggingMiddleware);
app.use("/cities", citiesRouter);
app.use("/types", typesRouter);
app.use("/partners", partnersRouter);
app.listen(port, () => {
    createLogger("app").info("Server is listening on port " + port + ".");
})
