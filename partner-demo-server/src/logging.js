const winston = require("winston");
const expressWinston = require("express-winston");

const commonLogOptions = {
    transports: [
        new winston.transports.Console()
    ],
    format: winston.format.json(),
}

module.exports = {
    loggingMiddleware: expressWinston.logger({
        ...commonLogOptions,
        meta: false,
        metaField: null,
        baseMeta: {category: "http"},
        msg: "HTTP {{req.method}} {{req.url}} with status {{res.statusCode}} in {{res.responseTime}}ms"
    }),
    createLogger: category => winston.createLogger({
        ...commonLogOptions,
        defaultMeta: {category: category},
    })
}
