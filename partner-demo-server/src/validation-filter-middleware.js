const {validationResult} = require("express-validator");

module.exports = function () {
    return (req, res, next) => {
        let errors = validationResult(req);
        if (!errors.isEmpty())
            res.status(400).json({errors: errors.array()});
        else
            next();
    }
}
