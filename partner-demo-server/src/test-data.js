const {createLogger} = require("./logging");
const log = createLogger("test-data");

const cities = ["Budapest", "Győr", "Pécs", "Szeged", "Debrecen"]
const businessForms = ["Kft", "Bt", "Zrt", "Nyrt"]
const partners = [
    {$name: "Magyar Televízió Zrt.", $type: 1, $city: 2},
    {$name: "Atrido Systems Kft.", $type: 2, $city: 1},
    {$name: "Innodox Technologies Zrt.", $type: 3, $city: 3},
    {$name: "Montana Tudásmenedzsment Kft.", $type: 1, $city: 2}
]

const stringListOf = entities =>
    entities
        .sort((a, b) => a.id-b.id)
        .map(entity => entity.name + "[" + entity.id + "]")
        .join(", ");

module.exports = {
    populateCities: (citiesRepository) => {
        return Promise.all(
            cities.map(name => citiesRepository.create({$name: name})
                .then(id => ({id, name})))
        )
            .then(cities => log.info(`cities=[ ${stringListOf(cities)} ]`));
    },
    populateTypes: (typesRepository) => {
        return Promise.all(
            businessForms.map(name =>
                typesRepository.create({$name: name})
                    .then(id => ({id, name})))
        )
            .then(types => log.info(`types=[ ${stringListOf(types)} ]`));
    },
    populatePartners: partnerRepository => {
        return Promise.all(
            partners.map(partner =>
                partnerRepository.create(partner)
                    .then(id => ({id, name: partner.$name})))
        )
            .then(partners => log.info(`partners=[ ${stringListOf(partners)} ]`))
    }
}
