module.exports = {
    select: function (selectStmt, db, params = {}) {
        return new Promise((resolve, reject) =>
            db.all(selectStmt, params, (err, rows) => {
                    if (err)
                        reject(err.message);
                    else
                        resolve(rows);
                }
            )
        )
    },
    insert: function (insertStmt, params, db) {
        return new Promise((resolve, reject) => {
            db.run(insertStmt, params, function (err) {
                if (err)
                    reject(err.message);
                else
                    resolve(this.lastID);
            });
        })
    },
    updateOrDelete: function (updateOrDeleteStmt, params, db) {
        return new Promise((resolve, reject) => {
            db.run(updateOrDeleteStmt, params, function (err) {
                if (err)
                    reject(err.message);
                else {
                    if (this.changes === 1)
                        resolve();
                    else
                        reject("conflict");
                }
            });
        })
    }
}
