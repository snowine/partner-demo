const {createLogger} = require('./logging');
const log = createLogger("schema");

module.exports = sqliteDb => {
    sqliteDb.serialize(function () {
        sqliteDb.run("PRAGMA foreign_keys = ON");
        schema.forEach(sql => {
            sqliteDb.run(sql);
            log.info(sql.replace(/\s+/gm, " "));
        })
    });
}

const schema = [
    `CREATE TABLE city (
    id INTEGER PRIMARY KEY,
    name TEXT UNIQUE NOT NULL,
    version INTEGER NOT NULL
                  )`,
    `CREATE TABLE business_form (
    id INTEGER PRIMARY KEY,
    name TEXT UNIQUE NOT NULL,
    version INTEGER NOT NULL
                           )`,
    `CREATE TABLE partner (
    id INTEGER PRIMARY KEY,
    name TEXT NOT NULL,
    business_form INTEGER NOT NULL REFERENCES business_form(id) ON DELETE RESTRICT,
    tax_number TEXT,
    registration_number TEXT,
    city INTEGER NOT NULL REFERENCES city(id) ON DELETE RESTRICT,
    address TEXT,
    phone TEXT,
    bank_account_no TEXT,
    note TEXT,
    version INTEGER NOT NULL
                     )`
]
